# Backpack Automatic Refresher Redux

### Before using this script, make sure you follow these instructions.

1. Make sure you have Python **3** installed. Get it here: https://www.python.org/downloads/
    - If you're on **Windows**, make sure the **"Add Python to environment variables"** or **"Add to PATH"** option is checked when installing Python **3**.
2. Install the requirements
    - If you're on **Windows**, double click on `install-requirements.bat`
    - Alternatively, you can install the requirements via console: `pip install -r requirements.txt` **OR** `pip install colorama requests`
3. Get the **ID64** of the inventory you want to refresh. 
    - You can find it at the end of a backpack.tf profile/inventory link. (e.g. https://backpack.tf/u/76561198087558444)
		- **7656119xxxxxxxxxx** is the **ID64**
		- You can also use https://steamid.xyz/ to find the **ID64** of a profile.
4. Get your user token. You can get it at https://backpack.tf/connections.

---

### Downloading and running the script

1. Download the archive and extract all files to a new folder: https://gitlab.com/carbonwind/backpack-automatic-refresher-redux/-/archive/main/backpack-automatic-refresher-redux-main.zip
2. On **Windows**, use `start-script.bat` to start the script.
    - **OR** via console (any OS): `python3 refreshbp.py` **OR** `python refreshbp.py`
3. Enter your ID64 and token when prompted.

---
### If you have issues with the script

#### I can't run the script!
- On Windows, reinstall Python and make sure **"Add to PATH"** is checked when installing Python **3**.
- Your version of Python **3** must be up to date, uninstall any old version.

#### Client error (status code 400 to 499)
- Make sure you have correctly entered your token and your ID64.
- If you get 429 (Too Many Requests), download the latest version of the refresher. Previous versions could potentially spam requests and rate limit you.

#### Server error (status code 500 to 599)
- This means there's an issue with backpack.tf's API server. The script automatically retries every 5 minutes if a server error is detected.

#### My backpack didn't update right away! This is too slow.
- This is normal and updating your backpack can take anywhere from a few minutes to a few hours in most cases.
- Make sure your backpack and Steam profile are not private, both must be public or your inventory will never update.
- When the backpack has been updated, you will see: 
	- The backpack was properly updated at most 3 minutes ago.

#### RecursionError: maximum recursion depth exceeded while calling a Python object
- Download the latest version of `refreshbp.py`. Previous versions used recursion which breaks the script after a considerable amount of attempts.

#### Invalid Syntax
- The requirements have not been installed or you accidentally saved the script as a web page. 

#### Other issues not listed here
- Make a post on the forums and include a screenshot of the error you got.
