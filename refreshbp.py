import requests, sys
from os import path
from colorama import Fore, Style, init
from time import sleep
from datetime import datetime

status_descriptions = {
    200: "OK",
    400: "Bad Request",
    401: "Unauthorized",
    403: "Forbidden",
    404: "Not Found",
    405: "Method Not Allowed",
    408: "Request Timeout",
    414: "URI Too Long",
    429: "Too Many Requests",
    500: "Internal Server Error",
    501: "Not Implemented",
    502: "Bad Gateway",
    503: "Service Unavailable",
    504: "Gateway Timeout",
    520: "Web server returned unknown error to Cloudflare",
    521: "Web server refuses connections from Cloudflare",
    522: "Timeout when Cloudflare contacted the origin web server",
    523: "Cloudflare cannot contact origin web server",
    524: "Response timeout occured when Cloudflare connected to origin web server",
    525: "SSL handshake failed between Cloudflare and the origin web server",
    526: "Cloudflare could not validate SSL certificate",
    527: "Railgun Listener to origin error",
    530: "Cloudflare error"
}

def get_token():
    script_dir = path.dirname(path.realpath(__file__))
    token_path = path.join(script_dir, "token.txt")
    
    # Check if token.txt already exists
    tokenfile_exists = path.exists(token_path)
    
    if tokenfile_exists:
        print(Fore.WHITE + "token.txt file exists.")
        with open(token_path, "r") as f:
            token = f.readline().strip()
    else:
        print(Fore.YELLOW + "token.txt not found.")
        token = input(Fore.WHITE + "Enter your backpack.tf token: ").strip()

    # Conditions to see if token is empty.
    if token == "" and tokenfile_exists:
        print(Fore.YELLOW + "Your token.txt file cannot be empty! Get your token at https://backpack.tf/connections")
        token = input(Fore.WHITE + "Enter your backpack.tf token: ").strip()
        with open(token_path, "w") as f:
            f.write(token)
            print(Fore.CYAN + "Updated token.txt file. You can edit this file if your token is invalid.")
        return get_token()
    elif token == "" and not tokenfile_exists:
        print(Fore.YELLOW + "Your token cannot be empty! Get it at https://backpack.tf/connections")
        return get_token()
    elif token != "" and not tokenfile_exists:
        with open(token_path, "w") as f:
            f.write(token)
            print(Fore.CYAN + "Created token.txt file. You can edit this file if your token is invalid.")
        return token
    else:
        print("token.txt not empty, starting refresh...")
        return token

def update_token():
    global token
    token = input(Fore.WHITE + "Enter your backpack.tf token: ").strip()

    script_dir = path.dirname(path.realpath(__file__))
    token_path = path.join(script_dir, "token.txt")

    with open(token_path, "w") as f:
        f.write(token)
        print(Fore.CYAN + "Updated token.txt file. You will have re-enter your token again if it is invalid.")

def start_refresh(p_id64, p_token):
    global updated
    result = request_refresh(p_id64, p_token)

    if result != "Error":
        if hasattr(result, 'text'):
            if result.text is not None:
                json = result.json()

                server_current_time = datetime.fromtimestamp(json['current_time'])
                last_attempt_time = datetime.fromtimestamp(json['last_update'])
                next_attempt_time = datetime.fromtimestamp(json['next_update'])
                last_snapshot_time = datetime.fromtimestamp(json['timestamp'])
                secs_since_lastsnapshot = (server_current_time - last_snapshot_time).total_seconds()

                if secs_since_lastsnapshot > 180:
                    print(f"{Style.BRIGHT}API server's current time:{Style.NORMAL} {server_current_time.strftime('%d %B, %Y %H:%M:%S')}", end="\n----------------\n")
                    print(f"{Style.BRIGHT}Last update attempt time:{Style.NORMAL} {last_attempt_time.strftime('%d %B, %Y %H:%M:%S')}")
                    print(f"{Style.BRIGHT}Next possible update:{Style.NORMAL} {next_attempt_time.strftime('%d %B, %Y %H:%M:%S')}")
                    print(f"{Style.BRIGHT}Latest snapshot time:{Style.NORMAL} {last_snapshot_time.strftime('%d %B, %Y %H:%M:%S')}", end="\n----------------\n")
                    
                    time_towait = (next_attempt_time - server_current_time).total_seconds()
                    print(f"{Style.BRIGHT}Seconds elapsed since last snapshot:{Style.NORMAL} {secs_since_lastsnapshot} (~{int(secs_since_lastsnapshot//60)} minutes | {int(secs_since_lastsnapshot//86400)} days ago)")

                    if loopuntilupdated:
                        if next_attempt_time > server_current_time:
                            print(f"Waiting {int(time_towait//60)} minutes and {int(time_towait % 60)} seconds before next refresh...")
                            sleep(time_towait)
                        else:
                            print(Fore.YELLOW + "Next update time lower than API's current server time, refreshing in 10 seconds.")
                            sleep(10) # Wait a bit to avoid spamming the API server if this happens twice or more in row.
                else:
                    updated = True
            else:
                print(Fore.YELLOW + f"Got empty response, retrying in {int(errorwait_time//60)} minutes.")
                sleep(errorwait_time)
        else:
            print(Fore.YELLOW + f"Expected response text not found, retrying in {int(errorwait_time//60)} minutes.")
            print(f"Unexpected response: {result}")
            sleep(errorwait_time)

def request_refresh(p_id64, p_token):
    global refresh_count
    url = f"https://backpack.tf/api/inventory/{p_id64}/refresh?token={p_token}"

    # User agent (you can set this to your browser's user agent)
    h = {'User-Agent': 'Backpack Autorefresh Redux 2024.05.01'}
    # Time to wait (in seconds) for a response
    t = 15

    try:
        refresh_count += 1
        print(Style.RESET_ALL + Style.BRIGHT)

        print(Fore.CYAN + f"Sending request to API server, {refresh_count} attempts so far.")
        r = requests.post(url, headers=h, timeout=t)
        pt = r.text
        
        if r.status_code == 200:
            print(Fore.GREEN + f"Got response, status code {r.status_code} ({status_descriptions[r.status_code]})", end="\n----------------\n")
            print(f"{Fore.WHITE + Style.BRIGHT}API response:{Style.NORMAL} {pt}")
            return r
        else: 
            if r.status_code in status_descriptions.keys():
                print(Fore.YELLOW + f"Got response, status code {r.status_code} ({status_descriptions[r.status_code]})", end="\n----------------\n")       
            else:
                print(Fore.YELLOW + f"Got response, status code {r.status_code}", end="\n----------------\n")
            
            print(f"{Fore.WHITE + Style.BRIGHT}API response:{Style.NORMAL} {pt}")
            
            if r.status_code >= 400 and r.status_code <= 499:
                if r.status_code == 401:
                    print(Fore.YELLOW + "Your token in token.txt appears to be invalid. Get it at https://backpack.tf/connections")
                    update_token()
                else:
                    print("Client error. Quitting...")
                    sys.exit()
            elif r.status_code >= 500 and r.status_code <= 599:
                print(f"Server error, waiting {int(errorwait_time//60)} minutes before retrying.")
                sleep(errorwait_time)
            else:
                print("Unexpected HTTP status code, report this issue if it persists. Quitting...")
                sys.exit()
                
            return "Error"
    except requests.exceptions.ReadTimeout:
        print(Fore.YELLOW + f"Timed out after {t} seconds, retrying.")
        return "Error"
         
refresh_count = 0
updated = False

# Settings
loopuntilupdated = True
errorwait_time = 300 # Time to wait (in seconds) when a server error is detected

init() # Initialize colorama

print(Fore.CYAN + "Welcome to the BP automatic refresher!" + Fore.WHITE + " Release date - 2024-05-01")
id64 = input("Enter your ID64: ").strip()
while len(id64) != 17 or not id64.startswith("7656"):
    print(Fore.YELLOW + "Invalid ID64!")
    id64 = input(Fore.WHITE + "Enter your ID64: ").strip()

token = get_token()

start_time = datetime.now()
if loopuntilupdated:
    while not updated:
        start_refresh(id64, token)
else:
    print(Fore.YELLOW + "WARNING: loopuntilupdated is currently set to False. The refresher will not attempt refreshing more than once.")
    start_refresh(id64, token)

end_time = datetime.now()
print(f"Finished refreshing {id64}. {Fore.GREEN + 'The backpack was properly updated at most 3 minutes ago.' if updated else Fore.RED + 'The backpack was not properly updated.'}")
print(f"{Fore.CYAN}Time taken to refresh: {end_time - start_time}")
